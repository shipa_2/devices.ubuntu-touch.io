---
name: "Volla Phone X"
deviceType: "phone"
buyLink: "https://volla.online/de/shop/"
tag: "promoted"
subforum: "100/volla-phone-x"
price:
  avg: 514

deviceInfo:
  - id: "cpu"
    value: "Octa-core ARM Cortex-A53 (4x 2.0 GHz + 4x 1.5 GHz cores)"
  - id: "chipset"
    value: "MediaTek Helio P23, MT6763V"
  - id: "gpu"
    value: "ARM Mali-G71 MP2 @ 770 MHz, 2 cores"
  - id: "rom"
    value: "64 GB, eMMC"
  - id: "ram"
    value: "4 GB, DDR3"
  - id: "android"
    value: "10.0"
  - id: "battery"
    value: "6200 mAh, Li-Polymer"
  - id: "display"
    value: '6.1" IPS, 720 x 1560 (283 PPI), V-notch, Rounded corners'
  - id: "rearCamera"
    value: "13MP (f/2.0, 1080p30 video) + 2MP (for bokeh/depth), PDAF, LED flash"
  - id: "frontCamera"
    value: "8MP"
  - id: "arch"
    value: "arm64"
  - id: "dimensions"
    value: "162.4 mm x 79 mm x 15.3 mm"
  - id: "weight"
    value: "280 g"
contributors:
  - name: "Hallo Welt Systeme UG"
    role: "Phone maker"
  - name: TheKit
    forum: https://forums.ubports.com/user/thekit
    photo: ""
  - name: Deathmist
    forum: https://forums.ubports.com/user/deathmist
    photo: https://forums.ubports.com/assets/uploads/profile/3171-profileavatar-1613145487767.png
externalLinks:
  - name: "Telegram"
    link: "https://t.me/utonvolla"
  - name: "Device Subforum"
    link: "https://forums.ubports.com/category/100/volla-phone-x"
  - name: "Report a bug"
    link: "https://github.com/HelloVolla/ubuntu-touch-beta-tests/issues"
  - name: "Device source"
    link: "https://gitlab.com/ubports/community-ports/android9/volla-phone-x/volla-yggdrasilx/-/tree/master"
  - name: "Kernel source"
    link: "https://github.com/HelloVolla/android_kernel_volla_mt6763/tree/halium-9.0-yggdrasilx"

seo:
  description: "Get your Volla Phone with latest version of Ubuntu Touch operating system, a private OS developed by hundreds of people."
  keywords: "Ubuntu Touch, VollaPhoneX, Volla Phone X, Privacy Phone"
---
