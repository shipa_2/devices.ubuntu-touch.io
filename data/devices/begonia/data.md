---
name: "Xiaomi Redmi Note 8 Pro"
deviceType: "phone"

deviceInfo:
  - id: "arch"
    value: "arm64"
  - id: "cpu"
    value: "Octa-core (2x Cortex-A76 + 6x Cortex-A55)"
  - id: "chipset"
    value: "Mediatek Helio G90T, MT6785V"
  - id: "gpu"
    value: "ARM Mali-G76 MC4"
  - id: "rom"
    value: "64 GB, 128 GB, 256 GB"
  - id: "ram"
    value: "4 GB, 6 GB, 8 GB"
  - id: "android"
    value: "Android 9.0"
  - id: "battery"
    value: "4500 mAh"
  - id: "display"
    value: '6.53" IPS, 1080 x 2340 (395 PPI), V-notch, rounded corners'
  - id: "rearCamera"
    value: "64 MP, 8 MP ultrawide, 2 MP macro, 2 MP depth"
  - id: "frontCamera"
    value: "20MP"
  - id: "dimensions"
    value: "161.4 x 76.4 x 8.8 mm (6.35 x 3.01 x 0.35 in)"
  - id: "weight"
    value: "200 g (7.05 oz)"
  - id: "releaseDate"
    value: "September 24th 2019"
externalLinks:
  - name: "Device Repositories"
    link: "https://gitlab.com/ubports/community-ports/android10/xiaomi-redmi-note-8-pro"
  - name: "Report a bug"
    link: "https://gitlab.com/ubports/community-ports/android10/xiaomi-redmi-note-8-pro/xiaomi-begonia/-/issues"
contributors:
  - name: TheKit
    forum: https://forums.ubports.com/user/thekit
    photo: ""
  - name: Rúben Carneiro
    forum: https://forums.ubports.com/user/rubencarneiro
    photo: ""
---
