import generateSearchIndex from "@logic/build/generateSearchIndex.js";

export async function get() {
  const data = await generateSearchIndex("devices-search-index");

  return {
    body: JSON.stringify(data)
  };
}
